const express = require('express');
const path = require('path');
const port = 3000;

const app = express();

const users = require('./routes/users');

app.use('/users', users);


app.get('/', (req, res) => {
  res.send('Home page');
});


app.listen(port, ()=> {
  console.log(`App listening at http://localhost:${port}`)
});
