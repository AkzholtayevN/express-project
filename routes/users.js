import {User} from "../src/shared/types";

const express = require('express');
const router = express.Router();

router.use(express.json());

let users: User;

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.status(200).json(users);
  res.send('respond with a resource');
  next();
});

router.get('/users/:id', (req, res) => {
  res.status(200).json(users.id);
  res.send('user page');
});

router.post('/users', (req, res) => {
  let user = {...req.body};
  users.push(user);
  res.status(201).json(user);
  res.send('user page');
});

router.put('/user/:id', (req, res) => {
  const index = users.findIndex(user => user.id === req.params.id);
  users[index] = req.body;
  res.json(users[index]);
  res.send('user page');
});


module.exports = router;
